package com.domain.app;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.reactive.error.ErrorWebFluxAutoConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@SpringBootApplication(exclude = ErrorWebFluxAutoConfiguration.class)
@OpenAPIDefinition(info = @io.swagger.v3.oas.annotations.info.Info(title = "APIs", version = "1.0", description = "APIs v1.0"))
public class Main {

  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }
}
