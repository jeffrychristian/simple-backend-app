package com.domain.app.service;

import com.domain.app.dto.request.RequestContext;
import com.domain.app.repository.CartRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CartService {

  final CartRepository cartRepository;

  public CartService(CartRepository cartRepository) {
    this.cartRepository = cartRepository;
  }

  public Mono addToCart(RequestContext context){
    return cartRepository.save(null);
  }

  public Mono list(RequestContext context, String userId){
    return cartRepository
        .findAllByUserIdAndDeleted(userId, false)
        .collectList();
  }

  public Mono remove(RequestContext context, String cartId, String activityId, String userId){
    return cartRepository.remove(cartId, activityId, userId);
  }
}