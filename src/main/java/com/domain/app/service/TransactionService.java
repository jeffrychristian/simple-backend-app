package com.domain.app.service;

import com.domain.app.dto.request.RequestContext;
import com.domain.app.repository.TransactionRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TransactionService {

  final TransactionRepository transactionRepository;

  public TransactionService(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public Mono checkout(RequestContext context){
    return null;
  }

  public Mono checkoutPay(RequestContext context){
    return null;
  }

  public Mono checkoutPayVerification(RequestContext context){
    return null;
  }
}
