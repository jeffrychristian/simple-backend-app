package com.domain.app.service;

import static com.domain.app.commons.enums.ResponseCode.PARTNER_ALREADY_EXIST;
import static com.domain.app.commons.enums.ResponseCode.SUCCESS;
import static com.domain.app.commons.enums.ResponseCode.USER_ALREADY_EXIST;
import static com.domain.app.libraries.utility.CommonHelper.buildBaseResponse;

import com.domain.app.dto.request.PartnerRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.dto.ressponse.BaseResponse;
import com.domain.app.entity.Partner;
import com.domain.app.libraries.exception.BusinessLogicException;
import com.domain.app.libraries.utility.JMapper;
import com.domain.app.repository.PartnerRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class PartnerService {

  final PartnerRepository partnerRepository;

  public PartnerService(PartnerRepository partnerRepository) {
    this.partnerRepository = partnerRepository;
  }

  public Mono<BaseResponse<Partner>> create(RequestContext context, PartnerRequest request){
    return partnerRepository.findByEmail(request.getEmail())
        .flatMap(partner -> Mono.error(new BusinessLogicException(PARTNER_ALREADY_EXIST)))
        .cast(Partner.class)
        .switchIfEmpty(Mono.defer(() -> {
          Partner partner = JMapper.map(request, Partner.class);
          partner.setVerified(false);
          partner.setDeleted(false);
          return partnerRepository.save(partner);
        }))
        .map(partner -> buildBaseResponse(SUCCESS, partner));
  }

  public Mono<BaseResponse<Partner>> update(RequestContext context,
      String id, PartnerRequest request){
    return partnerRepository.findById(id)
        .map(partner -> {
          partner.setFirstName(request.getFirstName());
          partner.setLastName(request.getLastName());
          partner.setBusinessName(request.getBusinessName());
          partner.setEmail(request.getEmail());
          partner.setPhone(request.getPhone());
          partner.setWebsite(request.getWebsite());
          partner.setPhoneAreaCode(request.getPhoneAreaCode());
          partner.setCountryId(request.getCountryId());
          partner.setPhoto(request.getPhoto());
          return partner;
        })
        .flatMap(partnerRepository::save)
        .map(partner -> buildBaseResponse(SUCCESS, partner));
  }

  public Mono<BaseResponse<String>> delete(RequestContext context, String id){
    return partnerRepository.findById(id)
        .flatMap(partner -> {
          partner.setDeleted(true);
          return partnerRepository.save(partner);
        })
        .map(partner -> buildBaseResponse(SUCCESS, SUCCESS.getCode()));
  }
}
