package com.domain.app.service;

import static com.domain.app.commons.enums.ResponseCode.SUCCESS;
import static com.domain.app.libraries.utility.CommonHelper.buildBaseResponse;

import com.domain.app.commons.enums.ResponseCode;
import com.domain.app.commons.fields.RequestContextField;
import com.domain.app.dto.request.LoginRequest;
import com.domain.app.dto.request.RegisterRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.dto.request.ResetPasswordRequest;
import com.domain.app.dto.ressponse.BaseResponse;
import com.domain.app.dto.ressponse.UserResponse;
import com.domain.app.entity.User;
import com.domain.app.libraries.exception.BusinessLogicException;
import com.domain.app.libraries.utility.AuthUtil;
import com.domain.app.libraries.utility.JMapper;
import com.domain.app.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserService {

  final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public Mono<ResponseEntity<BaseResponse<UserResponse>>> register(RequestContext context, RegisterRequest request){
    return userRepository.findByEmail(request.getEmail())
        .flatMap(m->Mono.error(new BusinessLogicException(ResponseCode.USER_ALREADY_EXIST)))
        .switchIfEmpty(Mono.defer(() -> userRepository.save(User.builder()
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .email(request.getEmail())
            .hash(AuthUtil.generateHash(request.getPassword()))
            .verificationCode(RandomStringUtils.randomNumeric(6))
            .verified(false)
            .build())))
        .cast(User.class)
        .map(u -> {
          String xAuth = AuthUtil.getXAuth(u.getId(), u.isVerified());
          UserResponse userResponse = JMapper.map(u, UserResponse.class);
          BaseResponse<UserResponse> response = buildBaseResponse(SUCCESS, userResponse);
          return ResponseEntity.ok().header(RequestContextField.X_AUTH, xAuth).body(response);
        });
  }

  public Mono<ResponseEntity<BaseResponse<UserResponse>>> login(RequestContext context, LoginRequest request){
    return userRepository.findByEmail(request.getEmail())
        .switchIfEmpty(Mono.error(new BusinessLogicException(ResponseCode.INVALID_EMAIL)))
        .flatMap(user -> {
          boolean passwordMatch = AuthUtil.isPasswordMatch(request.getPassword(), user.getHash());
          if (passwordMatch) {
            String xAuth = AuthUtil.getXAuth(user.getId(), user.isVerified());
            UserResponse userResponse = JMapper.map(user, UserResponse.class);
            BaseResponse<UserResponse> response = buildBaseResponse(SUCCESS, userResponse);
            return Mono.just(
                ResponseEntity.ok().header(RequestContextField.X_AUTH, xAuth).body(response));
          }
          return Mono.error(new BusinessLogicException(ResponseCode.INVALID_PASSWORD));
        });
  }

  public Mono<String> reset_password_by_email(RequestContext requestContext,
      ResetPasswordRequest request){
    return null;
  }
}