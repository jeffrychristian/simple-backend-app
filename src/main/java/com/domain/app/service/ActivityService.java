package com.domain.app.service;

import static com.domain.app.commons.enums.ResponseCode.DATA_NOT_EXIST;
import static com.domain.app.commons.enums.ResponseCode.SUCCESS;
import static com.domain.app.libraries.utility.CommonHelper.buildBaseResponse;

import com.domain.app.dto.request.ActivityRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.dto.request.SearchRequest;
import com.domain.app.entity.Activity;
import com.domain.app.libraries.exception.BusinessLogicException;
import com.domain.app.libraries.utility.JMapper;
import com.domain.app.repository.ActivityRepository;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ActivityService {

  final ActivityRepository activityRepository;

  public ActivityService(ActivityRepository activityRepository) {
    this.activityRepository = activityRepository;
  }

  public Mono auto_complete(RequestContext requestContext, String keyword){
    return activityRepository.autoComplete(keyword)
        .collectList();
  }

  public Mono<Page<Activity>> search(RequestContext context, SearchRequest request){
    return activityRepository.search(request);
  }

  public Mono detail(RequestContext context, String id){
    return activityRepository.findById(id)
        .switchIfEmpty(Mono.error(new BusinessLogicException(DATA_NOT_EXIST)))
        .map(activity -> buildBaseResponse(SUCCESS, activity));
  }

  public Mono create(RequestContext context, ActivityRequest request){
    Activity activity = JMapper.map(request, Activity.class);
    activity.setCode(new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()));
    activity.setDeleted(false);
    return activityRepository.save(activity);
  }

  public Mono update(RequestContext context, String id, ActivityRequest request){
    return activityRepository.findById(id)
        .switchIfEmpty(Mono.error(new BusinessLogicException(DATA_NOT_EXIST)))
        .doOnNext(activity -> {
          activity.setName(request.getName());
          activity.setImages(request.getImages());
          activity.setAvailabilityDate(request.getAvailabilityDate());
          activity.setTotalRating(request.getTotalRating());
          activity.setLongitude(request.getLongitude());
          activity.setLatitude(request.getLatitude());
          activity.setCountryId(request.getCountryId());
          activity.setRegionId(request.getRegionId());
          activity.setCityId(request.getCityId());
          activity.setAreaId(request.getAreaId());
          activity.setDescription(request.getDescription());
          activity.setDetail(request.getDetail());
          activity.setSubDetail(request.getSubDetail());
          activity.setActivityPlans(request.getActivityPlans());
          activity.setCategory(request.getCategory());
          activity.setSubCategory(request.getSubCategory());
          activity.setTags(request.getTags());
          activity.setPartnerId(request.getPartnerId());
        })
        .flatMap(activityRepository::save);
  }

  public Mono delete(RequestContext context, String id){
    return activityRepository.findById(id)
        .switchIfEmpty(Mono.error(new BusinessLogicException(DATA_NOT_EXIST)))
        .doOnNext(activity -> activity.setDeleted(true))
        .flatMap(activityRepository::save);
  }

}
