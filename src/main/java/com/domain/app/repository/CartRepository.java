package com.domain.app.repository;

import com.domain.app.entity.Cart;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface CartRepository extends ReactiveMongoRepository<Cart, String>, CartRepositoryCustom {

  Flux<Cart> findAllByUserIdAndDeleted(String userId, Boolean deleted);
}
