package com.domain.app.repository;

import com.domain.app.entity.Activity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ActivityRepository extends ReactiveMongoRepository<Activity, String>, ActivityRepositoryCustom {

}
