package com.domain.app.repository;

import reactor.core.publisher.Mono;

public interface CartRepositoryCustom {

  Mono remove(String cartId, String activityId, String userId);
}
