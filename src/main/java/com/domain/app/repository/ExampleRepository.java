package com.domain.app.repository;

import com.domain.app.entity.Example;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ExampleRepository extends ReactiveMongoRepository<Example, String> {

}
