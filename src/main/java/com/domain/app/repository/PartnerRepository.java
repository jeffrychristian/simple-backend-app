package com.domain.app.repository;

import com.domain.app.entity.Partner;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface PartnerRepository extends ReactiveMongoRepository<Partner, String> {

  Mono<Partner> findByEmail(String email);
}