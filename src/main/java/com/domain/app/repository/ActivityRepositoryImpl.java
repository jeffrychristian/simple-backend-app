package com.domain.app.repository;

import com.domain.app.dto.request.SearchRequest;
import com.domain.app.entity.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class ActivityRepositoryImpl implements ActivityRepositoryCustom {

  final ReactiveMongoTemplate reactiveMongoTemplate;

  public ActivityRepositoryImpl(
      ReactiveMongoTemplate reactiveMongoTemplate) {
    this.reactiveMongoTemplate = reactiveMongoTemplate;
  }

  @Override
  public Flux<Activity> autoComplete(String keyword) {
    return reactiveMongoTemplate.find(Query
            .query(Criteria
                .where("name").regex(keyword, "i")
                .and("deleted").is(false))
            .limit(10)
            .with(Sort.by(Direction.ASC, "name")),
        Activity.class);
  }

  @Override
  public Mono<Page<Activity>> search(SearchRequest request) {
    PageRequest pageRequest = PageRequest
        .of(request.getPage(), request.getSize(), Direction.fromString(request.getSortDirection()),
            request.getSortBy());
    Query query = Query
        .query(Criteria
            .where("name").regex(request.getKeyword(), "i")
            .and("deleted").is(false)).with(pageRequest);
    return reactiveMongoTemplate.find(query, Activity.class)
        .collectList()
        .flatMap(activities -> reactiveMongoTemplate.count(query, Activity.class)
        .map(count -> new PageImpl<>(activities, pageRequest, count)));

  }
}
