package com.domain.app.repository;

import com.domain.app.dto.request.SearchRequest;
import com.domain.app.entity.Activity;
import org.springframework.data.domain.Page;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ActivityRepositoryCustom {

  Flux<Activity> autoComplete(String keyword);
  Mono<Page<Activity>> search(SearchRequest request);
}
