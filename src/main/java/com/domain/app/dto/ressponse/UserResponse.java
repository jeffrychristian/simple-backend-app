package com.domain.app.dto.ressponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String phoneAreaCode;
  private String countryId;
  private String regionId;
  private String photo;
}
