package com.domain.app.dto.request;

import com.domain.app.entity.ActivityImage;
import com.domain.app.entity.ActivityPlan;
import com.domain.app.entity.AvailabilityDate;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityRequest {

  private String name;
  private List<ActivityImage> images;
  private AvailabilityDate availabilityDate;
  private Double totalRating;
  private Double longitude;
  private Double latitude;
  private String countryId;
  private String regionId;
  private String cityId;
  private String areaId;
  private Map<String, String> description;
  private Map<String, List<String>> detail;
  private Map<String, Map<String, List<String>>> subDetail;
  private List<ActivityPlan> activityPlans;

  private String category; //tour, etc
  private String subCategory; //A,B,C
  private List<String> tags; //A,B,C

  private String partnerId;
}
