package com.domain.app.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartnerRequest {

  private String firstName;
  private String lastName;
  private String businessName;
  private String email;
  private String phone;
  private String website;
  private String phoneAreaCode;
  private String countryId;
  private String photo;
}
