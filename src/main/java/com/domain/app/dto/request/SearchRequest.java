package com.domain.app.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequest {

  private String keyword;
  private int page;
  private int size;
  private String sortDirection;
  private String sortBy;
}
