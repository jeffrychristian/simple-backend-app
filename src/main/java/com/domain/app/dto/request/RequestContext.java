package com.domain.app.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestContext implements Serializable {

  private static final long serialVersionUID = 9181715730820410287L;

  @JsonProperty("X-Currency")
  private String currency;

  @JsonProperty("X-Forwarded-For")
  private String xForwardedFor;

  private String xAuth;
}
