package com.domain.app.controller;

import com.domain.app.dto.request.RequestContext;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ServerWebExchange;

@RestControllerAdvice
public class CommonRequestHandlerController {

  @ModelAttribute
  public RequestContext getMandatoryParameter(ServerWebExchange request) {
    return request.getAttribute("mandatory");
  }
}
