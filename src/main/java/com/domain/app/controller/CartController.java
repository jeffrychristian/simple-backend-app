package com.domain.app.controller;

import com.domain.app.commons.ApiPath;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.service.CartService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiPath.CART)
public class CartController {

  final CartService cartService;

  public CartController(CartService cartService) {
    this.cartService = cartService;
  }

  @PostMapping(ApiPath.ADD_TO_CART)
  public Mono<?> add_to_cart(@ModelAttribute RequestContext requestContext) {
    return cartService.addToCart(requestContext);
  }

  @GetMapping(ApiPath.LIST)
  public Mono<?> list(@ModelAttribute RequestContext requestContext,
      @RequestParam String userId) {
    return cartService.list(requestContext, userId);
  }

  @DeleteMapping(ApiPath.REMOVE)
  public Mono<?> remove(@ModelAttribute RequestContext requestContext,
      @RequestParam String cartId, @RequestParam String activityId, @RequestParam String userId) {
    return cartService.remove(requestContext, cartId, activityId, userId);
  }
}