package com.domain.app.controller;

import static com.domain.app.libraries.utility.CommonHelper.buildBaseResponse;

import com.domain.app.commons.ApiPath;
import com.domain.app.commons.enums.ResponseCode;
import com.domain.app.dto.request.LoginRequest;
import com.domain.app.dto.request.RegisterRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.dto.request.ResetPasswordRequest;
import com.domain.app.dto.ressponse.BaseResponse;
import com.domain.app.dto.ressponse.UserResponse;
import com.domain.app.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiPath.USER)
public class UserController {

  final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping(ApiPath.REGISTER)
  public Mono<ResponseEntity<BaseResponse<UserResponse>>> register(@ModelAttribute RequestContext requestContext,
      @RequestBody RegisterRequest request){
    //todo register by email, name, password | no need to verification
    return userService.register(requestContext, request);
  }

  @PostMapping(ApiPath.LOGIN)
  public Mono<ResponseEntity<BaseResponse<UserResponse>>> login(@ModelAttribute RequestContext requestContext,
      @RequestBody LoginRequest request){
    return userService.login(requestContext, request);
  }

  @PostMapping(ApiPath.RESET_PASSWORD)
  public Mono<BaseResponse<String>> reset_password_by_email(@ModelAttribute RequestContext requestContext,
      @RequestBody ResetPasswordRequest request){
    return userService.reset_password_by_email(requestContext, request)
        .map(data -> buildBaseResponse(ResponseCode.SUCCESS, data));
  }
}