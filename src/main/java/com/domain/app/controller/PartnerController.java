package com.domain.app.controller;

import com.domain.app.commons.ApiPath;
import com.domain.app.dto.request.PartnerRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.service.PartnerService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiPath.PARTNER)
public class PartnerController {

  final PartnerService partnerService;

  public PartnerController(PartnerService partnerService) {
    this.partnerService = partnerService;
  }

  @PostMapping(ApiPath.CREATE)
  public Mono<?> create(@ModelAttribute RequestContext context, @RequestBody PartnerRequest request){
    return partnerService.create(context, request);
  }

  @PutMapping(ApiPath.UPDATE + ApiPath.ID)
  public Mono<?> update(@ModelAttribute RequestContext context, @PathVariable String id,
      @RequestBody PartnerRequest request){
    return partnerService.update(context, id, request);
  }

  @DeleteMapping(ApiPath.DELETE)
  public Mono<?> delete(@ModelAttribute RequestContext context, @RequestParam String id){
    return partnerService.delete(context, id);
  }
}