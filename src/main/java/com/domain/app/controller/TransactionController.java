//package com.domain.app.controller;
//
//import com.domain.app.commons.ApiPath;
//import com.domain.app.dto.request.RequestContext;
//import com.domain.app.service.TransactionService;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import reactor.core.publisher.Mono;
//
//@RestController
//@RequestMapping(ApiPath.TRX)
//public class TransactionController {
//
//  final TransactionService transactionService;
//
//  public TransactionController(TransactionService transactionService) {
//    this.transactionService = transactionService;
//  }
//
//  @PostMapping
//  public Mono<?> checkout(@ModelAttribute RequestContext requestContext) {
//    return transactionService.checkout(requestContext);
//  }
//
//  @PostMapping
//  public Mono<?> checkout_pay(@ModelAttribute RequestContext requestContext) {
//    return transactionService.checkoutPay(requestContext);
//  }
//
//  @PostMapping
//  public Mono<?> checkout_pay_verification(@ModelAttribute RequestContext requestContext) {
//    return transactionService.checkoutPayVerification(requestContext);
//  }
//}
