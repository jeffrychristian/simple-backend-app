package com.domain.app.controller;

import com.domain.app.commons.ApiPath;
import com.domain.app.dto.request.ActivityRequest;
import com.domain.app.dto.request.RequestContext;
import com.domain.app.dto.request.SearchRequest;
import com.domain.app.service.ActivityService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiPath.ACTIVITY)
public class ActivityController {

  final ActivityService activityService;

  public ActivityController(ActivityService activityService) {
    this.activityService = activityService;
  }

  @GetMapping
  public Mono<?> auto_complete(@ModelAttribute RequestContext requestContext,
      @RequestParam String keyword){
    return activityService.auto_complete(requestContext, keyword);
  }

  @PostMapping(ApiPath.LIST)
  public Mono<?> search(@ModelAttribute RequestContext requestContext,
      @RequestBody SearchRequest request){
    return activityService.search(requestContext, request);
  }

  @GetMapping(ApiPath.ID)
  public Mono<?> detail(@ModelAttribute RequestContext requestContext,
      @RequestParam String id){
    return activityService.detail(requestContext, id);
  }

  @PostMapping(ApiPath.CREATE)
  public Mono<?> create(@ModelAttribute RequestContext context, @RequestBody ActivityRequest request){
    return activityService.create(context, request);
  }

  @PutMapping(ApiPath.UPDATE + ApiPath.ID)
  public Mono<?> update(@ModelAttribute RequestContext context, @PathVariable String id,
      @RequestBody ActivityRequest request){
    return activityService.update(context, id, request);
  }

  @DeleteMapping(ApiPath.DELETE)
  public Mono<?> delete(@ModelAttribute RequestContext context, @RequestParam String id){
    return activityService.delete(context, id);
  }
}
