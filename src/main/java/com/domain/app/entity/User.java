package com.domain.app.entity;

import com.domain.app.commons.CollectionName;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = CollectionName.USER)
public class User extends BaseMongo {

  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String phoneAreaCode;
  private String countryId;
  private String regionId;
  private String verificationCode;
  private String hash;
  private String photo;
  private boolean verified;

  @Builder
  public User(String id, Long version, Date createdDate, String createdBy, Date updatedDate,
      String updatedBy, String storeId, Boolean deleted, String firstName, String lastName,
      String email, String phone, String phoneAreaCode, String countryId, String regionId,
      String verificationCode, String hash, String photo,
      boolean verified) {
    super(id, version, createdDate, createdBy, updatedDate, updatedBy, storeId, deleted);
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.phoneAreaCode = phoneAreaCode;
    this.countryId = countryId;
    this.regionId = regionId;
    this.verificationCode = verificationCode;
    this.hash = hash;
    this.photo = photo;
    this.verified = verified;
  }
}
