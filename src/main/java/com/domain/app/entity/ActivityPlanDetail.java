package com.domain.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityPlanDetail {

  private String name;
  private PrimaryCurrencyPrice originalPrice;
  private PrimaryCurrencyPrice sellPrice;
}
