package com.domain.app.entity;

import com.domain.app.commons.CollectionName;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = CollectionName.CONTACT_US)
public class ContactUs extends BaseMongo {

  private String firstName;
  private String lastName;
  private String countryId;
  private String businessName;
  private String email;
  private String phone;
  private String phoneAreaCode;
  private String website;
  private String reason;
  private String message;

  @Builder
  public ContactUs(String id, Long version, Date createdDate, String createdBy,
      Date updatedDate, String updatedBy, String storeId, Boolean deleted,
      String firstName, String lastName, String countryId, String businessName,
      String email, String phone, String phoneAreaCode, String website, String reason,
      String message) {
    super(id, version, createdDate, createdBy, updatedDate, updatedBy, storeId, deleted);
    this.firstName = firstName;
    this.lastName = lastName;
    this.countryId = countryId;
    this.businessName = businessName;
    this.email = email;
    this.phone = phone;
    this.phoneAreaCode = phoneAreaCode;
    this.website = website;
    this.reason = reason;
    this.message = message;
  }
}
