package com.domain.app.entity;

import com.domain.app.commons.enums.ActivityPlanDateType;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AvailabilityDate {

  private ActivityPlanDateType activityPlanDateType;
  private List<Date> chosenDate;
  private Date rangeStartDate;
  private Date rangeEndDate;
}
