package com.domain.app.entity;

import com.domain.app.commons.CollectionName;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = CollectionName.ACTIVITY)
public class Activity extends BaseMongo{

  private String code; //product code
  private String name;
  private List<ActivityImage> images;
  private AvailabilityDate availabilityDate;
  private Double totalRating;
  private Double longitude;
  private Double latitude;
  private String countryId;
  private String regionId;
  private String cityId;
  private String areaId;
  private Map<String, String> description;
  private Map<String, List<String>> detail;
  private Map<String, Map<String, List<String>>> subDetail;
  private List<ActivityPlan> activityPlans;

  private String category; //tour, etc
  private String subCategory; //A,B,C
  private List<String> tags; //A,B,C

  private String partnerId;

  @Builder
  public Activity(String id, Long version, Date createdDate, String createdBy,
      Date updatedDate, String updatedBy, String storeId, Boolean deleted,
      String code, String name, List<ActivityImage> images,
      AvailabilityDate availabilityDate, Double totalRating, Double longitude,
      Double latitude, String countryId, String regionId, String cityId, String areaId,
      Map<String, String> description,
      Map<String, List<String>> detail,
      Map<String, Map<String, List<String>>> subDetail,
      List<ActivityPlan> activityPlans, String category, String subCategory,
      List<String> tags, String partnerId) {
    super(id, version, createdDate, createdBy, updatedDate, updatedBy, storeId, deleted);
    this.code = code;
    this.name = name;
    this.images = images;
    this.availabilityDate = availabilityDate;
    this.totalRating = totalRating;
    this.longitude = longitude;
    this.latitude = latitude;
    this.countryId = countryId;
    this.regionId = regionId;
    this.cityId = cityId;
    this.areaId = areaId;
    this.description = description;
    this.detail = detail;
    this.subDetail = subDetail;
    this.activityPlans = activityPlans;
    this.category = category;
    this.subCategory = subCategory;
    this.tags = tags;
    this.partnerId = partnerId;
  }
}
