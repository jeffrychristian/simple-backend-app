package com.domain.app.entity;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
public class PrimaryCurrencyPrice {

  //default is in rupiah, you need currency service to convert in another currency
  private String language; //id
  private String currency; //IDR
  private String symbol; //Rp
  private BigDecimal price;
}
