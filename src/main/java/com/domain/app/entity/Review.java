package com.domain.app.entity;

import com.domain.app.commons.CollectionName;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = CollectionName.REVIEW)
public class Review extends BaseMongo{

  private String activityId;
  private String userId;
  private String userPhoto;
  private String name;
  private Double rating;
  private String text;
  private List<String> photos;
}
