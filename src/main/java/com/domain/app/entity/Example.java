package com.domain.app.entity;

import java.util.Date;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = "example")
public class Example extends BaseMongo {

  private String json;
  private Binary data;
  private List<byte[]> bytes;

  @Builder
  public Example(String id, Long version, Date createdDate, String createdBy,
      Date updatedDate, String updatedBy, String storeId, Boolean deleted, String json,
      Binary data, List<byte[]> bytes) {
    super(id, version, createdDate, createdBy, updatedDate, updatedBy, storeId, deleted);
    this.json = json;
    this.data = data;
    this.bytes = bytes;
  }
}
