package com.domain.app.commons.fields;

public interface RequestContextField {
  String CURRENCY = "X-Currency";
  String X_FORWARDED_FOR = "X-Forwarded-For";
  String X_AUTH = "X-Auth";
}
