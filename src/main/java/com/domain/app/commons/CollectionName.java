package com.domain.app.commons;

public class CollectionName {

  public static final String USER = "user";
  public static final String TRANSACTION = "transaction";
  public static final String CART = "cart";
  public static final String ACTIVITY = "activity";
  public static final String REVIEW = "review";
  public static final String CONTACT_US = "contact_us";
}
