package com.domain.app.commons;

public class ApiPath {

  public static final String BASE_PATH = "/api";
  public static final String GATEWAY = BASE_PATH + "/data";
  public static final String ID = "/{id}";
  public static final String USER = GATEWAY + "/user";
  public static final String PARTNER = GATEWAY + "/partner";
  public static final String CART = GATEWAY + "/cart";
  public static final String TRX = GATEWAY + "/trx";
  public static final String ACTIVITY = GATEWAY + "/activity";
  public static final String RESET_PASSWORD = "/reset-password";
  public static final String REGISTER = "/register";
  public static final String LOGIN = "/login";
  public static final String CREATE = "/create";
  public static final String UPDATE = "/update";
  public static final String DELETE = "/delete";
  public static final String REMOVE = "/remove";
  public static final String ADD_TO_CART = "/add-to-cart";
  public static final String LIST = "/list";

  private ApiPath() {}
}