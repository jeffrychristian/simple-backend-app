package com.domain.app.commons;

import java.util.HashMap;
import java.util.Map;

public class CustomResponseMessage {

  public static final String TRIGGER_SYNC_SUCCESS = "Sync is now started";
  public static final String TRIGGER_SYNC_IN_PROGRESS = "Sync has been started, currently still in progress";
  public static final Map<String, String> START_DATE_MUST_NOT_BEFORE_NOW = new HashMap<>();
  public static final Map<String, String> START_DATE_MUST_BEFORE_END_DATE = new HashMap<>();
  public static final String FILE_NOT_IN_CSV = "File uploaded is not in CSV format";
  public static final String SYNC_IN_PROGRESS = "Sync currently in progress";
  public static final String SYNC_COMPLETED = "Sync completed";
  public static final String FILE_NOT_IMAGE = "File uploaded is not in image format";
  public static final String START_DATE_MUST_AFTER_TODAY_PLUS_ONE = "Start date must be minimum after today + 1 day";
  public static final String DESTINATION_SHOULD_BE_NOT_BLANK = "Please select a destination";
  public static final String PERIOD_DISPLAY_IS_NOT_CHOSEN = "Please select a Period of Display";
  public static final String IMAGE_SHOULD_BE_NOT_NULL = "Please choose an image";
  public static final String SEARCH_FILTER_SHOULD_BE_NOT_NULL = "Please select a filter";
  public static final String CARD_IS_ACTIVE = "The card is active, please choose other cards whose inactive for delete";
  public static final String WITHOUT_DESTINATION_TITLE_SIZE ="Title without destination max 40 character and min 1 character";
  public static final String WITHOUT_DESTINATION_TITLE_VALIDATION  ="Title without destination only alphanumeric and (,./!#@) symbol";
  public static final String GUEST_TOTAL_VALIDATION = "Guest max 32 and min 1 guest";
  public static final String ROOM_TOTAL_VALIDATION = "Room max 8 and min 1 room";
  public static final String MIN_ACTIVE_CARD = "Minimum active cards must be grater than two cards";
  public static final String MAX_ACTIVE_CARD = "Maximum active cards must be less than five cards";
  public static final String IS_HOTEL_NOW_SETTING = "IsDestination must be true if isHotelNow true";
  public static final String SEARCH_FILTER_INVALID = "Search filter invalid";
  public static final String SYSTEM_PARAMETER_NOT_EXIST = "System Parameter variable not found!";
  public static final String SYSTEM_PARAMETER_NOT_VALID = "System Parameter date value is not valid!";
  public static final String ROOM_GT_GUEST_VALIDATION = "Room total must be less than equal guest total";
  public static final String PERIOD_VALIDATION = "Can't change active status when the card have specified period";
  public static final String MOVE_ROOM_SUCCESS = "Successfully move rooms: ";
  public static final String MOVE_ALL_ROOM_SUCCESS = "Successfully move selected rooms";
  public static final String DUPLICATE_ORDER_NUMBER = "Please correct duplicate order entered";
  public static final String MISSING_ORDER_NUMBER = "Missing sequential order number";
  public static final String EXPORT_ROOMGROUP_SUCCESS = "Preparing .csv document in Export page. Might take up to 10 minutes.";
  public static final String EXPORT_ROOMGROUP_FAILED = "Error exporting document. Please try again.";


  private CustomResponseMessage(){}

  static {
    START_DATE_MUST_NOT_BEFORE_NOW
        .put("en", "Start date / time can not before current date / time");
    START_DATE_MUST_NOT_BEFORE_NOW
        .put("id", "Tanggal / waktu mulai tidak boleh sebelum tanggal / waktu saat ini");
    START_DATE_MUST_BEFORE_END_DATE.put("en", "Start date / time must be before end date / time");
    START_DATE_MUST_BEFORE_END_DATE
        .put("id", "Tanggal / waktu mulai harus sebelum tanggal / waktu akhir");
  }
}
