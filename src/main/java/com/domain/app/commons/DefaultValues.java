package com.domain.app.commons;

public class DefaultValues {

  private DefaultValues() {
  }

  public static final String SYSTEM = "system";
  public static final String NOTES = "Put all mandatory parameter";
  public static final int SIZE = 100;
  public static final String ARGUMENT_FIELD = "{field}";
  public static final String PROPERTY_TYPE_ID = "HOTEL";
  public static final String ZERO_MONGO_ID = "000000000000000000000000";

  // Time milisecond
  public static final int ONE_WEEK = 604800;
  public static final int ONE_DAY = 86400;
  public static final int ONE_HOUR= 3600;
  public static final int TWELVE_HOUR = 43200;
}
