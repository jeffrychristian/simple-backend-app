package com.domain.app.commons.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
public class WebClientProperties {

  private String connectionName;
  private String host;
  private Integer connectTimeout;
  private Integer readTimeout;
  private Integer maxConnections;
  private Integer maxIdleTime;
  private Integer maxLifeTime;
}
