package com.domain.app.commons;

public class CacheKey {

  private static final String PREFIX = "com.domain.app-";

  private CacheKey() {
  }
}
