package com.domain.app.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LanguageCode {
  EN_US("en-US"),
  IN_ID("in-ID");

  private final String value;

  public static boolean isValueOf(String code) {
    try {
      LanguageCode.valueOf(code);
      return true;
    } catch(Exception e) {
      return false;
    }
  }
}
