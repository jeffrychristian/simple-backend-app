package com.domain.app.commons.enums;

public enum ActivityType {

  TOUR, TICKET, UMROH
}
