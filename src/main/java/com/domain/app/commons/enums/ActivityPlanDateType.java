package com.domain.app.commons.enums;

public enum ActivityPlanDateType {

  CHOSEN_DATE, //list of date 1,3,10,31
  RAGE_DATE, //start 2021-01-01 -  end 2020-02-28

}
