package com.domain.app.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseCode {
  SUCCESS("SUCCESS", "SUCCESS"),
  FAILED("FAILED", "FAILED"),
  SYSTEM_ERROR("SYSTEM_ERROR", "Contact our team"),
  BAD_REQUEST("BAD_REQUEST", "Please fill in mandatory parameter"),
  INTERNAL_OUTBOUND_ERROR("INTERNAL_OUTBOUND_ERROR", "Error call to internal service"),
  THIRD_PARTY_ERROR("THIRD_PARTY_ERROR", "Content that does not conform to JSON syntax"),
  THIRD_PARTY_DOWN("THIRD_PARTY_DOWN", "Contact our team"),
  DUPLICATE_DATA("DUPLICATE_DATA", "Duplicate data"),
  USER_ALREADY_EXIST("USER_ALREADY_EXIST", "User already exist"),
  PARTNER_ALREADY_EXIST("PARTNER_ALREADY_EXIST", "Partner already exist, please use another email address"),
  DATA_NOT_EXIST("DATA_NOT_EXIST", "No data exist"),
  INVALID_EMAIL("INVALID_EMAIL", "Invalid email"),
  INVALID_PASSWORD("INVALID_PASSWORD", "Invalid password"),
  RUNTIME_ERROR("RUNTIME_ERROR", "Runtime Error"),
  BIND_ERROR("BIND_ERROR", "Please fill in mandatory parameter"),
  MISMATCH_VENDOR("MISMATCH_VENDOR", "Different vendor found"),
  MIME_ERROR("MIME_ERROR", "Mime Error"),
  ARGUMENTS_NOT_VALID("ARGUMENTS_NOT_VALID", "Method arguments not valid"),
  MISSING_PARAMETER("MISSING_PARAMETER", "%s parameter is missing"),
  INVALID_JSON_FORMAT("INVALID_JSON_FORMAT", "Invalid JSON format"),
  FILE_SIZE_TOO_LARGE("FILE_SIZE_TOO_LARGE", "File size is too large"),
  INVALID_RANGE_DATE("INVALID_RANGE_DATE", "Invalid range date"),
  OPTIMISTIC_LOCKING_FAILED("OPTIMISTIC_LOCKING_FAILED",
      "Row was updated or deleted by another transaction, please use latest data version"),
  INVALID_INPUT_DATA("INVALID_DATA_INPUT", "Invalid data input"),
  ILLEGAL_STATE_EXCEPTION("ILLEGAL_STATE_EXCEPTION", "Illegal State Exception"),
  METHOD_ARGUMENTS_NOT_VALID("METHOD_ARGUMENTS_NOT_VALID", "method_arguments_not_valid");

  private final String code;
  private final String message;

  public static boolean isValueOf(String code) {
    try {
      ResponseCode.valueOf(code);
      return true;
    } catch(Exception e) {
      return false;
    }
  }
}
