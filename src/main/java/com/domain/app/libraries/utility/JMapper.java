package com.domain.app.libraries.utility;

import java.util.List;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory.Builder;

public class JMapper {

  private static final MapperFactory mf = new Builder().build();
  private static final MapperFacade m;

  public JMapper() {
  }

  static {
    m = mf.getMapperFacade();
  }

  public static <S, C> C map(S source, Class<C> clazz) {
    return source == null ? null : m.map(source, clazz);
  }

  public static <S, C> List<C> mapList(Iterable<S> source, Class<C> clazz) {
    return source == null ? null : m.mapAsList(source, clazz);
  }
}
