package com.domain.app.libraries.utility;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.domain.app.commons.fields.RequestContextField;
import com.domain.app.dto.ressponse.UserResponse;
import org.joda.time.DateTime;
import reactor.netty.http.server.HttpServerRequest;

public class AuthUtil {

  public static String generateHash(String pwd){
    return BCrypt.withDefaults().hashToString(10, pwd.toCharArray());
  }

  public static boolean isPasswordMatch(String pwd, String hash){
    return BCrypt.verifyer().verify(pwd.toCharArray(), hash).verified;
  }

  public static String getXAuth(String id, boolean verified){
    return JWT.create()
        .withExpiresAt(DateTime.now().plusMonths(1).toDate())
        .withClaim("id", id)
        .withClaim("verified", verified)
        .sign(Algorithm.none());
  }
}
