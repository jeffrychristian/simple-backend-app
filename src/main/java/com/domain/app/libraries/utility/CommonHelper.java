package com.domain.app.libraries.utility;

import com.domain.app.commons.enums.ResponseCode;
import com.domain.app.dto.ressponse.BaseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.joda.time.DateTime;

public class CommonHelper {
  private static ObjectMapper mapper = new ObjectMapper();

  private CommonHelper() {
  }

  public static <T> BaseResponse<T> buildBaseResponse(ResponseCode responseCode, T data) {
    return buildBaseResponse(responseCode.getCode(), responseCode.getMessage(), null, data);
  }

  public static <T> BaseResponse<T> buildBaseResponse(String code, String message,
      List<String> errors, T data) {
    return (BaseResponse<T>) BaseResponse.builder().code(code).message(message).errors(errors)
        .serverTime(new DateTime().toDate()).data(data).build();
  }
}
