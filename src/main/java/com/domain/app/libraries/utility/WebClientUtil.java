package com.domain.app.libraries.utility;

import org.apache.commons.collections4.MapUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WebClientUtil {

  public <T> Mono<ResponseEntity<T>> get(WebClient webClient, String url, HttpHeaders headers, MultiValueMap<String, String> queryParams, Class<T> responseClass){
    return webClient
        .get()
        .uri(uri -> {
          uri.path(url);
          if (MapUtils.isNotEmpty(queryParams)) {
            uri.queryParams(queryParams);
          }
          return uri.build();
        })
        .headers(httpHeaders -> httpHeaders.addAll(headers))
        .retrieve()
        .toEntity(responseClass);
  }

  public <T> Mono<ResponseEntity<T>> get(WebClient webClient, String url, HttpHeaders headers, ParameterizedTypeReference<T> bodyTypeReference){
    return webClient
        .get()
        .uri(uri -> uri.path(url).build())
        .headers(httpHeaders -> httpHeaders.addAll(headers))
        .retrieve()
        .toEntity(bodyTypeReference);
  }

  public <T> Mono<ResponseEntity<T>> post(WebClient webClient, String url, HttpHeaders headers, String jsonPayload, MultiValueMap<String, String> queryParams, ParameterizedTypeReference<T> bodyTypeReference){
    return webClient
        .post()
        .uri(uri -> {
          uri.path(url);
          if (MapUtils.isNotEmpty(queryParams)) {
            uri.queryParams(queryParams);
          }
          return uri.build();
        })
        .headers(httpHeaders -> httpHeaders.addAll(headers))
        .bodyValue(jsonPayload)
        .retrieve()
        .toEntity(bodyTypeReference);
  }
}
