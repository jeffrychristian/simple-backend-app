package com.domain.app.libraries.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class JSONUtil {

  private static final ObjectMapper mapper = new ObjectMapper();

  private JSONUtil() {
  }

  public static <T> String toJson(T data) throws JsonProcessingException {
    return mapper.writeValueAsString(data);
  }

  public static <T> T fromJson(String jsonInString, Class<T> clazz) throws IOException {
    return mapper.readValue(jsonInString, clazz);
  }
}
