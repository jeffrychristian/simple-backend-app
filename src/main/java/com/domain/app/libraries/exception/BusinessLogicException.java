package com.domain.app.libraries.exception;

import com.domain.app.commons.enums.ResponseCode;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class BusinessLogicException extends RuntimeException {

  private static final long serialVersionUID = 4411412561685442694L;

  private String code;
  private String message;
  private List<String> arguments;

  public BusinessLogicException(String code, String message) {
    super();
    this.setCode(code);
    this.setMessage(message);
  }

  public BusinessLogicException(String code, String message, List<String> arguments) {
    super();
    this.setCode(code);
    this.setMessage(message);
    this.setArguments(arguments);
  }

  public BusinessLogicException(ResponseCode responseCode) {
    super();
    code = responseCode.getCode();
    message = responseCode.getMessage();
    setCode(code);
    setMessage(message);
  }

  public BusinessLogicException(ResponseCode responseCode, List<String> arguments) {
    super();
    code = responseCode.getCode();
    message = responseCode.getMessage();
    this.setArguments(arguments);
  }

  @Override
  public String getMessage() {
    return message;
  }

}
