package com.domain.app.libraries.exception;

import com.domain.app.commons.enums.ResponseCode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

public class ErrorHandler {

  public static List<String> bindException(Throwable throwable){
    BindException be = (BindException) throwable;
    List<FieldError> bindErrors = be.getFieldErrors();
    List<String> errors = new ArrayList<>();
    for (FieldError fieldError : bindErrors) {
      errors.add(fieldError.getField() + " " + fieldError.getDefaultMessage());
    }
    return errors;
  }

  public static List<String> optimisticLockingFailureException(Throwable throwable){
    List<String> errors = new ArrayList<>();
    errors.add(ResponseCode.OPTIMISTIC_LOCKING_FAILED.getMessage());
    return errors;
  }

  public static List<String> duplicateException(Throwable throwable){
    String key = StringUtils.substringBetween(throwable.getMessage(), "dup key: { : ", " }");
    List<String> errors = new ArrayList<>();
    errors.add(key);
    return errors;
  }

  public static List<String> methodArgumentNotValidException(Throwable throwable){
    MethodArgumentNotValidException manve = (MethodArgumentNotValidException) throwable;
    List<String> errors = new ArrayList<>();
    String field = "{field}";
    for (FieldError fieldError : manve.getBindingResult().getFieldErrors()) {
      if (StringUtils.isBlank(fieldError.getDefaultMessage())) continue;
      if (fieldError.getDefaultMessage().contains(field)) {
        errors.add(fieldError.getDefaultMessage().replace(field, fieldError.getField()));
      } else {
        errors.add(fieldError.getField() + " " + fieldError.getDefaultMessage());
      }
    }
    for (ObjectError fieldError : manve.getBindingResult().getGlobalErrors()) {
      if (StringUtils.isBlank(fieldError.getDefaultMessage())) continue;
      if (fieldError.getDefaultMessage().contains(field)) {
        errors.add(fieldError.getDefaultMessage().replace(field, fieldError.getObjectName()));
      } else {
        errors.add(fieldError.getObjectName() + " " + fieldError.getDefaultMessage());
      }
    }
    errors.sort(Comparator.naturalOrder());
    return errors;
  }

  public static BusinessLogicException businessLogicException(Throwable throwable) {
    return (BusinessLogicException) throwable;
  }
}
