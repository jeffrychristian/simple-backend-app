package com.domain.app.libraries.configuration;

import com.domain.app.commons.fields.RequestContextField;
import com.domain.app.dto.request.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class InterceptorRequest implements WebFilter {

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    final HttpHeaders headers = exchange.getRequest().getHeaders();

    RequestContext requestContext = RequestContext.builder()
        .xForwardedFor(headers.getFirst(RequestContextField.X_FORWARDED_FOR))
        .currency(headers.getFirst(RequestContextField.CURRENCY))
        .xAuth(headers.getFirst(RequestContextField.X_AUTH))
        .build();
    exchange.getAttributes().put("mandatory", requestContext);

    return chain.filter(exchange);
  }
}

