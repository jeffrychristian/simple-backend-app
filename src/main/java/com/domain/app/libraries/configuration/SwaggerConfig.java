package com.domain.app.libraries.configuration;

import static springfox.documentation.builders.PathSelectors.regex;

import com.domain.app.commons.fields.RequestContextField;
import java.util.Arrays;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

@Configuration
public class SwaggerConfig {

  //change this for version change
  private static final String VERSION = "1.0.0-1";

  private static final String STORE_ID = "DOMAIN.COM";
  private static final String REQUEST_ID = "23123123";
  private static final String SERVICE_ID = "LOGIN";
  private static final String CHANNEL_ID = "WEB";
  private static final String USERNAME = "testuser";
  private static final String CURRENCY = "IDR";
  private static final String ACCEPT_LANGUAGE = "id";
  private static final String FORWARD_FOR = "127.0.0";
  private static final String TRUE_CLIENT_IP = "127.0.0";

  @Bean
  public Docket init() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.domain.app.controller"))
        .paths(regex("/.*"))
        .build()
        .globalRequestParameters(Arrays.asList(
//            new RequestParameterBuilder()
//                .name(RequestContextField.STORE_ID)
//                .description("store id")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(STORE_ID)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.CHANNEL_ID)
//                .description("channel id")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(CHANNEL_ID)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.REQUEST_ID)
//                .description("request id")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(REQUEST_ID)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.SERVICE_ID)
//                .description("service id")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(SERVICE_ID)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.USERNAME)
//                .description("username")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(USERNAME)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.ACCEPT_LANGUAGE)
//                .description("language")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(ACCEPT_LANGUAGE)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.CURRENCY)
//                .description("currency")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(CURRENCY)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.X_FORWARDED_FOR)
//                .description("x forwarded for")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(FORWARD_FOR)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.TRUE_CLIENT_IP)
//                .description("true client IP")
//                .in(ParameterType.HEADER)
//                .required(false)
//                .query(builder -> builder.defaultValue(TRUE_CLIENT_IP)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build(),
//            new RequestParameterBuilder()
//                .name(RequestContextField.X_AUTH)
//                .description("auth token")
//                .in(ParameterType.HEADER)
//                .query(builder -> builder.defaultValue(null)
//                    .model(m -> m.scalarModel(ScalarType.STRING)).allowEmptyValue(true)).build()
        ))
        .apiInfo(apiInfo())
        .genericModelSubstitutes(DeferredResult.class, ResponseEntity.class);
  }

  @Bean
  UiConfiguration uiConfig() {
    return UiConfigurationBuilder.builder()
        .displayRequestDuration(true)
        .validatorUrl(null)
        .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("API")
        .description("API Documentation")
        .version(VERSION)
        .license("Apache License Version 2.0")
        .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
        .build();
  }
}
