package com.domain.app.libraries.configuration;

import com.domain.app.commons.enums.ResponseCode;
import com.domain.app.libraries.exception.BusinessLogicException;
import com.domain.app.libraries.exception.ErrorHandler;
import com.domain.app.libraries.utility.CommonHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Configuration
@Order(-2)
public class GlobalErrorHandler implements ErrorWebExceptionHandler {

  private final ObjectMapper objectMapper;

  public GlobalErrorHandler(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public Mono<Void> handle(ServerWebExchange exchange, Throwable t) {
    if (t instanceof BindException) {
      log.error("BindException -> ", t);
      return response(exchange, ResponseCode.BIND_ERROR, ErrorHandler.bindException(t), HttpStatus.BAD_REQUEST);
    } else if (t instanceof MethodArgumentNotValidException) {
      log.error("MethodArgumentNotValidException -> ", t);
      return response(exchange, ResponseCode.ARGUMENTS_NOT_VALID, ErrorHandler.methodArgumentNotValidException(t), HttpStatus.BAD_REQUEST);
    } else if (t instanceof DuplicateKeyException) {
      log.error("DuplicateException -> ", t);
      return response(exchange, ResponseCode.DUPLICATE_DATA, ErrorHandler.duplicateException(t), HttpStatus.BAD_REQUEST);
    } else if (t instanceof OptimisticLockingFailureException) {
      log.error("OptimisticLockingFailureException -> ", t);
      return response(exchange, ResponseCode.OPTIMISTIC_LOCKING_FAILED, ErrorHandler.optimisticLockingFailureException(t), HttpStatus.BAD_REQUEST);
    } else if(t instanceof BusinessLogicException){
      log.error("BusinessLogicException -> ", t);
      BusinessLogicException e = ErrorHandler.businessLogicException(t);
      return response(exchange, e.getCode(), e.getMessage(), null, HttpStatus.OK);
    } else if (t instanceof RuntimeException) {
      log.error("RuntimeException -> ", t);
      return response(exchange, ResponseCode.RUNTIME_ERROR, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    log.error("Exception -> ", t);
    return response(exchange, ResponseCode.SYSTEM_ERROR, null, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private Mono<Void> response(ServerWebExchange serverWebExchange, ResponseCode rc,
      List<String> errors, HttpStatus httpStatus) {
    return response(serverWebExchange, rc.getCode(), rc.getMessage(), errors, httpStatus);
  }

  private Mono<Void> response(ServerWebExchange serverWebExchange, String code, String msg,
      List<String> errors, HttpStatus httpStatus) {
    DataBufferFactory bufferFactory = serverWebExchange.getResponse().bufferFactory();
    DataBuffer dataBuffer;
    try {
      dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(
          CommonHelper.buildBaseResponse(code, msg, errors, null)));
    } catch (JsonProcessingException e) {
      dataBuffer = bufferFactory.wrap("".getBytes());
    }
    serverWebExchange.getResponse().setStatusCode(httpStatus);
    serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
    return serverWebExchange.getResponse().writeWith(Mono.just(dataBuffer));
  }
}
