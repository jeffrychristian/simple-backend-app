package com.domain.app.libraries.configuration;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClientSettings.Builder;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import com.mongodb.connection.ClusterConnectionMode;
import com.mongodb.connection.ClusterSettings;
import com.mongodb.connection.ConnectionPoolSettings;
import com.mongodb.connection.ServerSettings;
import com.mongodb.connection.SocketSettings;
import com.mongodb.connection.netty.NettyStreamFactoryFactory;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import io.netty.channel.nio.NioEventLoopGroup;
import java.time.Clock;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.annotation.PreDestroy;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.mongo.MongoClientSettingsBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@ComponentScan(basePackages = "com.domain.app.repository")
@ComponentScan(basePackages = "com.domain.app.service")
@EnableReactiveMongoRepositories(value = "com.domain.app.repository")
public class ServiceConfiguration {

  private static final String DESCRIPTION = "mongodb";

  private final NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();

  @Bean
  public MongoClientSettings mongoClientSettings(ReactiveMongoProperties reactiveMongoProperties){
    final String[] hosts = reactiveMongoProperties.getHost().split(",");
    final String[] ports = reactiveMongoProperties.getPort().split(",");
    List<ServerAddress> serverAddresses = IntStream.range(0, hosts.length)
        .mapToObj(i -> new ServerAddress(hosts[i], Integer.parseInt(ports[i])))
        .collect(toList());
    Builder builder = MongoClientSettings.builder()
        .writeConcern(WriteConcern.ACKNOWLEDGED)
        .applyToConnectionPoolSettings(connectionPool -> connectionPool
            .applySettings(ConnectionPoolSettings.builder()
                .maxConnectionLifeTime(reactiveMongoProperties.getMaxConnectionLifeTime(),
                    MILLISECONDS)
                .maxConnectionIdleTime(reactiveMongoProperties.getMaxConnectionIdleTime(),
                    MILLISECONDS)
                .maxWaitTime(reactiveMongoProperties.getMaxWaitTime(), MILLISECONDS)
                .maxSize(reactiveMongoProperties.getMaxConnectionPerHost())
                .minSize(reactiveMongoProperties.getMinConnectionsPerHost())
                .build()))
        .readPreference(ReadPreference.valueOf(reactiveMongoProperties.getReadPreference()))
        .applyToClusterSettings(cluster -> cluster.applySettings(ClusterSettings.builder()
            .hosts(serverAddresses)
            .mode(hosts.length == 1 ? ClusterConnectionMode.SINGLE : ClusterConnectionMode.MULTIPLE)
            .serverSelectionTimeout(reactiveMongoProperties.getServerSelectionTimeout(),
                MILLISECONDS)
            .build()))
        .applyToServerSettings(serverSetting -> serverSetting
            .applySettings(ServerSettings.builder()
                .heartbeatFrequency(reactiveMongoProperties.getHeartbeatFrequency(), MILLISECONDS)
                .minHeartbeatFrequency(reactiveMongoProperties.getMinHeartbeatFrequency(),
                    MILLISECONDS)
                .build()))
        .applyToSocketSettings(b -> b.applySettings(SocketSettings.builder()
            .connectTimeout(reactiveMongoProperties.getConnectTimeout(), MILLISECONDS)
            .readTimeout(reactiveMongoProperties.getReadTimeout(), MILLISECONDS)
            .receiveBufferSize(reactiveMongoProperties.getReceiveBufferSize())
            .sendBufferSize(reactiveMongoProperties.getSendBufferSize()).build()))
        .retryReads(true)
        .retryWrites(true)
        .applicationName(DESCRIPTION)
        .streamFactoryFactory(NettyStreamFactoryFactory.builder()
            .eventLoopGroup(eventLoopGroup).build());
    if (StringUtils.isNotBlank(reactiveMongoProperties.getUsername()) && Objects
        .nonNull(reactiveMongoProperties.getPassword()) &&
        StringUtils.isNotBlank(reactiveMongoProperties.getAuthenticationDatabase())) {
      builder.credential(MongoCredential
          .createScramSha1Credential(reactiveMongoProperties.getUsername(),
              reactiveMongoProperties.getAuthenticationDatabase(),
              reactiveMongoProperties.getPassword()));
    }
    return builder.build();
  }

  @Bean
  public MongoClient mongoClient(ReactiveMongoProperties reactiveMongoProperties){
    return MongoClients.create(mongoClientSettings(reactiveMongoProperties));
  }

  @Bean
  public ReactiveMongoDatabaseFactory reactiveMongoDatabaseFactory(
      ReactiveMongoProperties reactiveMongoProperties) {
    return new SimpleReactiveMongoDatabaseFactory(mongoClient(reactiveMongoProperties),
        reactiveMongoProperties.getDatabase());
  }

  @Bean
  public MongoClientSettingsBuilderCustomizer mongoClientSettingsBuilderCustomizer(ReactiveMongoProperties
      reactiveMongoProperties) {
    return clientSettingsBuilder -> mongoClientSettings(reactiveMongoProperties);
  }


  @PreDestroy
  public void shutDownEventLoopGroup() {
    eventLoopGroup.shutdownGracefully();
  }


  @Bean
  public ObjectMapper createObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, true);
    objectMapper.configure(DeserializationFeature.ACCEPT_FLOAT_AS_INT, false);
    return objectMapper;
  }

  @Bean(name = "reactiveMongoTemplate")
  public ReactiveMongoTemplate reactiveMongoTemplate(
      ReactiveMongoDatabaseFactory reactiveMongoDatabaseFactory, MongoConverter converter) {
    return new ReactiveMongoTemplate(reactiveMongoDatabaseFactory, converter);
  }

  @Bean
  public Clock clock() {
    return Clock.systemUTC();
  }

  @Bean
  public AuditorAware<String> stringAuditorAware() {
    return () -> Optional.of("system");
  }
}
