package com.domain.app.libraries.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "mongodb")
public class ReactiveMongoProperties {

  /**
   * Default port used when the configured port is {@code null}.
   */
  public static final int DEFAULT_PORT = 27017;

  /**
   * Default URI used when the configured URI is {@code null}.
   */
  public static final String DEFAULT_URI = "mongodb://localhost/db_name";

  /**
   * Mongo server host. Cannot be set with URI.
   */
  private String host;

  /**
   * Mongo server port. Cannot be set with URI.
   */
  private String port = null;

  /**
   * Mongo database URI. Cannot be set with host, port and credentials.
   */
  private String uri;

  /**
   * Database name.
   */
  private String database;

  /**
   * Authentication database name.
   */
  private String authenticationDatabase;

  /**
   * GridFS database name.
   */
  private String gridFsDatabase;

  /**
   * Login user of the mongo server. Cannot be set with URI.
   */
  private String username;

  /**
   * Login password of the mongo server. Cannot be set with URI.
   */
  private char[] password;

  /**
   * Fully qualified name of the FieldNamingStrategy to use.
   */
  private Class<?> fieldNamingStrategy;
  private Integer maxConnectionPerHost;
  private Integer minConnectionsPerHost;
  private Integer connectTimeout;
  private Integer readTimeout;
  private Integer maxWaitTime;
  private Integer socketTimeout;
  private Integer heartbeatFrequency;
  private Integer maxConnectionIdleTime;
  private Integer maxConnectionLifeTime;
  private Integer minHeartbeatFrequency;
  private Integer maxQueueSize;
  private Integer waitQueueSize;
  private Integer serverSelectionTimeout;
  private Integer receiveBufferSize;
  private Integer sendBufferSize;
  private String readPreference;
}
